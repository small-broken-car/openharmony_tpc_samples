# text-encoding单元测试用例

该测试用例基于OpenHarmony系统下，采用[原库测试用例](https://github.com/MikeMcl/bignumber.js/tree/master/test)
进行单元测试

单元测试用例覆盖情况

|                  接口名                   |是否通过	|备注|
|:--------------------------------------:|:---:|:---:|
|            absoluteValue()             |    pass        |       |
|            BigNumber(data)             |pass   |        |
|                clone()                 |pass   |        |
|           isBigNumber(data)            |pass   |        |
|              config(data)              |pass   |        |
|            comparedTo(data)            |pass   |        |
|          decimalPlaces(data)           |pass  |     |
|            dividedBy(data)             |   pass  |          |
|        dividedToIntegerBy(data)        | pass |  |
|         exponentiatedBy(data)          | pass  |       |
|           integerValue(data)           |  pass |          |
|            isMethods(data)             |  pass |          |
|       BigNumber.min.apply(data)        | pass  |          |
|              BigNumber.max.apply(data)               | pass  |          |
|           min(data)           |  pass |          |
|            max(data)             |  pass |          |
|              isNaN()               | pass  |          |
|              minus(data)               | pass  |          |
|              valueOf()               | pass  |          |
|              times(data)              |  pass |       |
|              multipliedBy(data)              | pass  |       |
|              modulo(data)              | pass  |          |
|               negated()               | pass  |         |
|               plus(data)               | pass  |       |
|               precision(data)               | pass  |       |
|            random()             |  pass |       |
|            shiftedBy(data)             |  pass |       |
|            squareRoot(data)            | pass  |          |
|            sum(data)             |  pass |       |
|          toExponential(data)           | pass  |          |
|             toFixed(data)              | pass |       |
|             toFormat(data)              |  pass |       |
|            toFraction(data)            | pass  |          |
|             toNumber(data)             |pass   |          |
|           toPrecision(data)            | pass  |      |
|             toString(data)             |  pass |       |