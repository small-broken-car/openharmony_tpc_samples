## v4.0.7

1. 本库是基于EventEmitter3源库v4.0.7版本代码进行OpenHarmony环境接口验证的三方库。
2. EventEmitter3是一款高性能事件侦听器，支持添加监听事件，监听一次性事件，发送事件，移除事件，统计监听事件的个数，统计监听事件的名称。
3. 包管理工具由npm切换为ohpm
4. 适配DevEco Studio:  4.0 Release(4.0.3.513)
5. 适配SDK: API10 Release(4.0.10.10)



