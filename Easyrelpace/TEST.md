## easy-replace单元测试用例

该测试用例基于OpenHarmony系统下，采用[原库测试用例](https://github.com/codsen/codsen/tree/main/packages/easy-replace/test) 进行单元测试

### 单元测试用例覆盖情况

|接口名 | 是否通过 |备注|
|---|---|---|
|version|pass|
|er|pass|
|Opts|pass|

## leven单元测试用例

该测试用例基于OpenHarmony系统下，采用[原库测试用例](https://github.com/sindresorhus/leven/blob/main/test.js) 进行单元测试

### 单元测试用例覆盖情况

|接口名 | 是否通过 |备注|
|---|---|---|
|leven|pass|
