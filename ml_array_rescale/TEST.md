## escape_latex单元测试用例

该测试用例基于OpenHarmony系统下，参照[原库测试用例](https://github.com/mljs/array/tree/master/packages/array-rescale/src/__tests__/rescale.test.js)进行单元测试

### 单元测试用例覆盖情况

|      接口名      |    是否通过	     |备注|
|:-------------:|:------------:|:---:|
| rescale(data) |     pass     |       |