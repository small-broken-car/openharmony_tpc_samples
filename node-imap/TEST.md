## IMAP单元测试用例

该测试用例基于OpenHarmony系统下，进行单元测试

### 单元测试用例覆盖情况

|接口名 | 是否通过 |备注|
|---|---|---|
|serverSupports|pass|
|getBoxes|pass|
|addBox|pass|
|dexBox|pass|
|renameBox|pass|
|subscribeBox|pass|
|unsubscribeBox|pass|
|getSubscribeBox|pass|
|status|pass|
|openBox_closeBox|pass|
|fetch_closeBox|pass|
|addFlags_closeBox|pass|
|setFlags_closeBox|pass|
|delFlags_closeBox|pass|
|addKeywords_closeBox|pass|
|setKeywords_closeBox|pass|
|delKeywords_closeBox|pass|
|expunge_closeBox|pass|
|search_closeBox|pass|
|sort_closeBox|pass|
|copy_closeBox|pass|
|move_closeBox|pass|
|append_closeBox|pass|
|end|pass|