###1.2.1
1.适配DevEco Studio 3.1 Beta1版本

###1.2.0
1.适配api9

### 1.1.1
1.新增rpc和websocket使用样例

### 1.1.0
1.样例适配的系统版本是OpenHarmony API Version 8，且将原来工程转为hvigor工程。

### 1.0.0

1.样例适配的系统版本是OpenHarmony API Version 5~7。

2.使用三方库protobufjs版本支持@5.0.3

3.支持序列化和反序列化的样例

4.支持buffer读写的样例

