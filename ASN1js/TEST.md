# @fortanix/asn1js单元测试用例

该测试用例基于OpenHarmony系统下进行的@fortanix/asn1js单元测试用例
进行单元测试

单元测试用例覆盖情况

| 接口名 |                    是否通过	                     |备注|
|:---:|:--------------------------------------------:|:---:|
|  BaseBlock   |                     pass                     |       |
|  OctetString   |                     pass                     |       |
|  Boolean   |                     pass                     |       |
|  toJSON   |                     pass                     |       |
|  toBER   |                     pass                     |       |
|  Integer   |                     pass                     |       |
|  GeneralizedTime   |                     pass                     |       |
|  ObjectIdentifier   |                     pass                     |       |