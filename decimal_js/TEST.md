## ml-array-max单元测试用例

该测试用例基于OpenHarmony系统下进行单元测试

### 单元测试用例覆盖情况

| 接口名                                         | 是否通过 |备注|
|---------------------------------------------|------|---|
| add(n: Decimal.Value)                       | pass |
| div(n: Decimal.Value)                       | pass |
| sub(n: Decimal.Value)                       | pass |
| mul(n: Decimal.Value)                       | pass |
| pow(n: Decimal.Value)                       | pass |
| abs(n: Decimal.Value)                       | pass |
| sin()                                       | pass |
| cos()                                       | pass |
| tan()                                       | pass |
| acos()                                      | pass |
| floor()                                     | pass |
| ceil()                                      | pass |
| round()                                     | pass |
| toBinary(significantDigits?: number)        | pass |
| toExponential(decimalPlaces?: number)       | pass |
| toFixed(decimalPlaces?: number)             | pass |
| toPrecision(significantDigits?: number)     | pass |
| toFraction(max_denominator?: Decimal.Value) | pass |
| isZero()                                    | pass |